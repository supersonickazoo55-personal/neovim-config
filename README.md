# neovim-config

## About
This is my personal configuration for neovim. Feel free to use it.

## External dependencies
*Packages are named after Arch Linux packages*  

- [Packer](https://aur.archlinux.org/packages/nvim-packer-git)
- python-lsp-server (for Python suggestions)
- ripgrep (for Telescope grep feature)
- [python-pylsp-mypy](https://aur.archlinux.org/packages/python-pylsp-mypy) (Static type check)

## How to Install

1. Clone repository into your `~/.config/nvim` directory
2. Open a new neovim instance and run :PackerSync command to install plugins.  
